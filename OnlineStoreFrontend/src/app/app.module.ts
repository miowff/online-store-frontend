import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import {  RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './components/nav-bar/nav-bar.component';
import { ProductCard } from './components/product-card/produuct-card.component';
import { ListOfProducts } from './components/list-of-elements/list-of-elements.component';
import { LoginComponent } from './components/login/login.component';
import { CartComponent } from './components/cart/cart.component';
import { ProductsOfCategoryComponent } from './components/products-of-category/products-of-category.component';
import { AllProductsComponent } from './components/all-products/all-products.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { AuthGuard } from './Guards/auth.guard';
import { RegisterComponent } from './components/register/register.component';
import { CartElementComponent } from './components/cart-element/cart-element.component';
import { FilterPipe } from './Pipes/filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ProductCard,
    ListOfProducts,
    LoginComponent,
    CartComponent,
    ProductsOfCategoryComponent,
    AllProductsComponent,
    UserProfileComponent,
    RegisterComponent,
    CartElementComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      { path: '', component: AllProductsComponent },
      { path: 'cart', component: CartComponent },
      { path: 'products-of-category/:category', component: ProductsOfCategoryComponent },
      { path: 'user-profile', component: UserProfileComponent, canActivate: [AuthGuard] },
      { path: 'register', component: RegisterComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
