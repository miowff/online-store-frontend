import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../Models/getproduct';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: Product[], args: any[]): Product[] {
    const sortField = args[0];
    const sortDirection = args[1];

    let multiplyer = 1;

    if (sortDirection === 'desc') {
      multiplyer = -1;
    }

    value.sort((a: any, b: any) => {
      if (a[sortField] < b[sortField]) {
        return -1 * multiplyer;
      }
      else if (a[sortField] > b[sortField]) {
        return 1 * multiplyer;
      }
      else {
        return 0;
      }
    })
    return value;
  }

}
