import { Product } from "./getproduct";

export class CartElement {
  product: Product;
  count: number;
  totalPrice: number;

  constructor(product: Product, count: number) {

    this.count = count;
    this.product = product;
    this.totalPrice = count * product.Price;
  }
}
