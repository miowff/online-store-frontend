export class Product {
  Id!: number;
  Title!: string;
  Price!: number;
  Category!: string;
  Description!:string
}
