import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  authUser(userEmail: string) {
    localStorage.setItem('Token', userEmail)
  }

}
