import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../Models/getproduct';

@Injectable({
  providedIn: 'root'
})
export class GetproductsService  {
  constructor(private _httpClient: HttpClient) { }

  getAllProducts(): Observable<Product[]> {
    return this._httpClient.get<Product[]>('Data/AllProducts.json');
  }

  getAllCategoryProducts(category: string): Observable<Product[]> {
    return this._httpClient.get<Product[]>(`Data/${category}.json`);
  }

  getProductById(id: number): Observable<Product> {
    return this._httpClient.get<Product>('Data/TestProduct.json')
  }

}
