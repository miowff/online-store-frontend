import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CartElement } from '../Models/CartElement';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  public cartItems: CartElement[] = [];
  public cartItemsList = new BehaviorSubject<any>([]);

  constructor() { }

  getProducts() {
    return this.cartItemsList.asObservable();
  }

  setProduct(product: CartElement) {
    this.cartItems.push(product);
    this.cartItemsList.next(product);
  }

  addToCart(product: CartElement) {
    this.cartItems.push(product);
    this.cartItemsList.next(this.cartItems);
  }

  getTotalPrice():number {
    let total = 0;
    this.cartItems.map((a: CartElement) => {
      total += a.count * a.product.Price;
    })
    return total;
  }

  removeItem(cartItem: CartElement) {
    this.cartItems.map((a: CartElement, index: any) => {
      if (cartItem === a) {
        this.cartItems.splice(index, 1);
      }
    })
  }

  removeAll() {
    this.cartItems = [];
    this.cartItemsList.next(this.cartItems);
  }
}
