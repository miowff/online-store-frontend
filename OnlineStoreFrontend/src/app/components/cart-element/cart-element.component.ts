import { Component, Input, OnInit } from '@angular/core';
import { count } from 'rxjs';
import { CartElement } from '../../Models/CartElement';
import { CartService } from '../../Services/Cart.service';

@Component({
  selector: 'app-cart-element',
  templateUrl: './cart-element.component.html',
  styleUrls: ['./cart-element.component.css']
})
export class CartElementComponent implements OnInit {

  @Input() cartElement!: CartElement;

  constructor(private _cartService: CartService) { }

  ngOnInit(): void {
  }

  removeFromCart() {
    this._cartService.removeItem(this.cartElement);
  }

  incrementCount() {
    this.cartElement.count++;
    this.cartElement.totalPrice += this.cartElement.product.Price;
  }

  decrementCount() {
    if (this.cartElement.count <= 1) {
      this.removeFromCart();
    }
    this.cartElement.count--;
    this.cartElement.totalPrice -= this.cartElement.product.Price;
  }
}
