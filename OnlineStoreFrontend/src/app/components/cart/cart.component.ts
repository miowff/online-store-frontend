import { Component, OnInit } from '@angular/core';
import { CartElement } from '../../Models/CartElement';
import { CartService } from '../../Services/Cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cartTotal!: number;

  cartElements: Array<CartElement> = [];

  constructor(private _cartService: CartService) { }

  ngOnInit(): void {
    this._cartService.getProducts().subscribe(cartElements => {
      this.cartElements = cartElements;
    });
    let totalTopay = 0;
    this.cartElements.forEach(element => {
      totalTopay += element.totalPrice;
    })
  }

  emptyCart() {
    this._cartService.removeAll();
  }
}
