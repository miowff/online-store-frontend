import { Component, Input } from '@angular/core'
import { Product } from '../../Models/getproduct'


@Component({
  selector: "list-of-products",
  templateUrl: 'list-of-elements.component.html',
  styleUrls: ['list-of-elements.component.css']
})

export class ListOfProducts  {
  @Input() Products!: Array<Product>;
  searchString!: string;
  SortByParam: string = 'desc';

  private _allProductsOfList: Product[] = [];

  search() {
    if (this.searchString.length === 0) {
      this.Products = this._allProductsOfList;
      return;
    }
    this._allProductsOfList = this.Products;
    this.Products = this.Products.filter(product => product.Title.startsWith(this.searchString));
  }
}
