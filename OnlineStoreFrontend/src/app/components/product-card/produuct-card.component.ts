import { Component, Input } from '@angular/core'
import { CartElement } from '../../Models/CartElement';
import { Product } from '../../Models/getproduct'
import { CartService } from '../../Services/Cart.service';

@Component(
  {
    selector: "element-card",
    templateUrl: 'product-card.component.html',
    styleUrls: ['product-card.component.css']
  })

export class ProductCard {

  count: number = 0;
  @Input() product!: Product

  constructor(private _messanger: CartService) { }

  addToCart() {
    this._messanger.addToCart(new CartElement(this.product, this.count));
  }

}
