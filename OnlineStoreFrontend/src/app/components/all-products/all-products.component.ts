import { Component, OnInit } from '@angular/core';
import { Product } from '../../Models/getproduct';
import { GetproductsService } from '../../Services/Products.service';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.css']
})
export class AllProductsComponent implements OnInit {
  public allProducts: Product[] = [];
  constructor(private _productService: GetproductsService) { }

  ngOnInit(): void {
    this._productService.getAllProducts().subscribe(products => {
      this.allProducts = products;
    })
  }

}
