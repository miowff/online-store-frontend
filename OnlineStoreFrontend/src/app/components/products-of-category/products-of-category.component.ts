import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../../Models/getproduct';
import { GetproductsService } from '../../Services/Products.service';

@Component({
  selector: 'app-products-of-category',
  templateUrl: './products-of-category.component.html',
  styleUrls: ['./products-of-category.component.css']
})
export class ProductsOfCategoryComponent implements OnInit {
  public category!: string;
  public productsOfCategory: Product[] = [];
  constructor(private _route: ActivatedRoute, private _productService: GetproductsService) { }

  ngOnInit(): void {
    this.category = this._route.snapshot.params['category'];
    this._productService.getAllCategoryProducts(this.category).subscribe(products => {
      this.productsOfCategory = products
    });
    console.log(this.productsOfCategory);
  }
}
