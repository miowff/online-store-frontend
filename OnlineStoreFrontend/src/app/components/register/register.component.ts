import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registrationForm!: FormGroup;
  user: any = {};

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createRegistrationForm();
  }

  createRegistrationForm() {
    this.registrationForm = this._formBuilder.group({
      userName: ['Your name', Validators.required],
      email: ['Your email', Validators.email],
      password: [null, [Validators.required, Validators.minLength(4)]],
      mobile: [null, [Validators.required, Validators.maxLength(10)]]
    })
  }

  onSubmit() {
  }

}
